#include <iostream>


int main() {

	bool found = false;
	int i = 1;

	while (found == false) {
		int num = ((i*i) + i) / 2;
		int factors = 0;

		
		for (int j = 1; j <= num; j++) {

			if (num % j == 0) {
				factors++;				
			}

		}
		
		if (factors >= 500) {
			std::cout << num << std::endl;
			found = true;
		}

		i++;
	}

	std::cin.get();


	return 0;
}