#include <iostream>
#include <vector>


int main() {

	std::vector<int> digits;

	int numOfDigitsInSequence = 0;
	long long curDigit = 0;
	int maxSequence = 0;
	int maxSequenceDigit = 0;
	digits.push_back(0);
	digits.push_back(1);

	for (int i = 2; i < 1000000; i++) {
		numOfDigitsInSequence = 0;
		curDigit = i;

		std::cout << i << std::endl;

		//std::cout << digits[1] << std::endl;

		while (true) {

			if (curDigit % 2 == 0) {

				curDigit = curDigit / 2;
				numOfDigitsInSequence += 1;

			}
			else {

				curDigit = ((curDigit * 3) + 1);
				numOfDigitsInSequence += 1;

			}

			if (curDigit < i) {

				numOfDigitsInSequence += digits[curDigit];
				//std::cout << i << "    " << numOfDigitsInSequence << std::endl;
				digits.push_back(numOfDigitsInSequence);

				if (numOfDigitsInSequence > maxSequence) {
					maxSequence = numOfDigitsInSequence;
					maxSequenceDigit = i;
				}

				break;

			}


		}


	}

	std::cout << " \n\n\n\n  " << maxSequenceDigit << std::endl;

	std::cin.get();

	

	return 1;

}