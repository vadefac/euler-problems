#include <iostream>

int main() {

	int term1 = 1;
	int term2 = 2;
	int sum = 0;

	while (term2 < 5000000) {
		int temp = term1;		
		
		term1 = term2;
		term2 = term1 + temp;

		

		if (term1 % 2 == 0) {
			sum += term1;
		}
	}

	std::cout << sum;

	std::cin.get();

	return 1;
}