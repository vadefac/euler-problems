#include <iostream>
#include <vector>
#include <algorithm>


int main() {

	int product = 1206;
	int largestPalindrome = 0;
	for (int i = 999; i > 100; i--) {

		for (int j = 999; j > 100; j--) {

			product = i*j;		

			int k = product;
			int length = 0;
			while (k != 0) {

				length++;
				k /= 10;
			}

			int x = 1;
			int ten = 10;
			std::vector<int> vectorBackward;
			std::vector<int> vectorForward;

			while (x <= length) {

				vectorBackward.push_back(((product % ten) / (ten / 10)));
				x += 1;
				ten *= 10;
			}
			vectorForward = vectorBackward;
			std::reverse(vectorForward.begin(), vectorForward.end());

			for (int ij = 0; ij < vectorBackward.size(); ij++) {


				if (vectorBackward[ij] != vectorForward[ij]) {
					
					break;
				}
				if (ij == vectorBackward.size() - 1) {

					if (product > largestPalindrome) {
						largestPalindrome = product;
						std::cout << largestPalindrome << "\n";
					}
					
				}

			}


		}

	}

	std::cout << "Done";
	std::cin.get();

	return 1;
}

