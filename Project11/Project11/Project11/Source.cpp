#include <iostream>
#include <fstream>
#include <vector>


int main() {

	std::fstream inputStream;	
	std::vector< std::vector<int> > vector2d;

	inputStream.open("GridNumbers.txt");

	int a;
	
	int j = 0;
	while (j < 20) {
		int i = 0;
		std::vector<int> numbers;
		while (i < 20) {
			inputStream >> a;
			numbers.push_back(a);
			i++;
		}
		vector2d.push_back(numbers);
		j++;
	}
	


	int maxNum = 0;

	for (int i = 0; i < 20; i++) {
		int tempNum = 0;
		for (int j = 0; j < 16; j++) {
			tempNum = vector2d[i][j] * vector2d[i][j + 1] * vector2d[i][j + 2] * vector2d[i][j + 4];
			if (tempNum > maxNum) {
				maxNum = tempNum;
			}
			
		}
	}

	for (int i = 0; i < 16; i++) {
		int tempNum = 0;
		for (int j = 0; j < 20; j++) {
			tempNum = vector2d[i][j] * vector2d[i+1][j] * vector2d[i+2][j] * vector2d[i+3][j];
			if (tempNum > maxNum) {
				maxNum = tempNum;
			}

		}
	}

	for (int i = 0; i < 16; i++) {
		int tempNum = 0;
		for (int j = 0; j < 16; j++) {
			tempNum = vector2d[i][j] * vector2d[i + 1][j+1] * vector2d[i + 2][j+2] * vector2d[i + 3][j+3];
			if (tempNum > maxNum) {
				maxNum = tempNum;
			}

		}
	}

	std::cout << vector2d[19][19] << std::endl;

	for (int i = 0; i < 17; i++) {
		int tempNum = 0;
		for (int j = 19; j >=3; j--) {
			tempNum = vector2d[i][j] * vector2d[i + 1][j - 1] * vector2d[i + 2][j - 2] * vector2d[i +3][j - 3];
			if (tempNum > maxNum) {
				maxNum = tempNum;
			}

		}
	}
	
	std::cout << maxNum;

	inputStream.close();



	std::cin.get();
}