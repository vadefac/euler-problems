#include <iostream>
/*
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/

int main() {

	int i = 21;
	bool found = false;
	while (found == false) {

		for (int j = 1; j <= 20; j++) {

			if (i % j != 0) {
				break;
			}
			
			if (j == 20) {
				std::cout << i;
				found = true;
			}
			

		}

		i++;
	}



	std::cout << "Done";
	std::cin.get();
	return 1;
}