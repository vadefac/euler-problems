/*
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
*/
#include <iostream>

int main() {

	int numOfPrimes = 0;
	int number = 2;
	bool done = false;
	while (done == false) {

		for (int i = 2; i < number; i++) {

			if (number % i == 0) {			

				break;
			}

			if (i == number - 1) {

				numOfPrimes += 1;
				std::cout << number << "\n";

				if (numOfPrimes == 10000) {
					std::cout << "10001 prime number is: " << number;
					done = true;
				}

			}

		}		

		number += 1;

	}


	std::cin.get();
	return 1;
}